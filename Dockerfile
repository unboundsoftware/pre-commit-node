FROM amd64/node:22.14.0@sha256:32ad68e28d3eb06c388fc9d87f2143c2df157d4e99ade9024189513238c0b838

ENV PATH=${PATH}:/root/.local/bin

RUN apt update && \
    apt install -y python3 python3-pip ruby ruby-dev git pipx && \
    rm -rf /var/lib/apt/lists/* && \
    pipx install pre-commit
